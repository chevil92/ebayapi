<?php

namespace App\Services;

use Illuminate\Support\Facades\Http;
use Illuminate\Http\Request;

class EbayApiService
{
    private $api_endpoint = 'http://svcs.sandbox.ebay.com/services/search/FindingService/v1?';
    private $api_app_id = 'WandoInt-217b-42d8-a699-e79808dd505e';
    
    private function default_request_data(){
        return [
            'OPERATION-NAME' => 'findItemsByKeywords',
            'SERVICE-VERSION' => '1.0.0',
            'SECURITY-APPNAME' => $this->api_app_id,
            'RESPONSE-DATA-FORMAT' => 'json',
    
        ];
    } 

    public function get($data){
        return Http::get($this->api_endpoint, array_merge($this->default_request_data(), $data))->json();
    }

    public function get_limited_fields($data){
        $response = $this->get($data);
        // var_dump($response);
        $result = [];
        foreach($response['findItemsByKeywordsResponse'][0]['searchResult'][0]['item'] as $item){
            $result[] = [
                'item_id' => $item['itemId'][0]??'',
                'click_out_link' => $item['viewItemURL'][0]??'',
                'main_photo_url' => $item['galleryURL'][0]??'',
                'price' => $item['sellingStatus'][0]['currentPrice'][0]['__value__']??'',
                'price_currency' => $item['sellingStatus'][0]['currentPrice'][0]['@currencyId']??'',
                'shipping_price' => $item['shippingInfo'][0]['shippingServiceCost'][0]['__value__']??'',
                'title' => $item['title'][0]??'',
                'valid_until' => $item['sellingStatus'][0]['timeLeft'][0]??'',
            ];
        }
        return $result;
    }
}
