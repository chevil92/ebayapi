<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\EbayApiService;

class SearchController extends Controller
{
    public function search(Request $request){
        // if not isset $request->input('keyword') throw exception 
        $ebay_api = new EbayApiService();
        $data_for_request = ['keywords' => $request->input('keywords')];
        $filter_index = 0;
        if($request->input('price_min')){
                $data_for_request['itemFilter('.$filter_index.').name'] = 'MinPrice';
                $data_for_request['itemFilter('.$filter_index.').value'] = $request->input('price_min');
                $data_for_request['itemFilter('.$filter_index.').paramName'] = 'Currency';
                $data_for_request['itemFilter('.$filter_index.').paramValue'] = 'USD';
            
            $filter_index++;
        }
        if($request->input('price_max')){
            $data_for_request['itemFilter('.$filter_index.').name'] = 'MaxPrice';
            $data_for_request['itemFilter('.$filter_index.').value'] = $request->input('price_max');
            $data_for_request['itemFilter('.$filter_index.').paramName'] = 'Currency';
            $data_for_request['itemFilter('.$filter_index.').paramValue'] = 'USD';
        }

        return $ebay_api->get_limited_fields($data_for_request);
    }
}
